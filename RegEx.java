package SourceCodeVideo;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegEx {
    public static void main(String[] args) {
        Pattern pattern1 = Pattern.compile("(^[a-z])|(\\d)");
        Matcher matcher1 = pattern1.matcher("a b c d 1 2 3 4");
        System.out.println(matcher1.find());
        Matcher matcher2 = pattern1.matcher("A B C D 1 2 3 4");
        while (matcher2.find()) {
            System.out.println(matcher2.group());
        }
        Pattern pattern2 = Pattern.compile("(\\d+).*\\1");
        Matcher matcher3 = pattern2.matcher("2017 year, 2018 year, 2019 year ");
        while (matcher3.find()) {
            System.out.println(matcher3.group(1));
        }


            //Квантификаторы
        System.out.println(Pattern.compile("10{2,4}").matcher("1000").find());
        System.out.println(Pattern.compile("10?").matcher("10").find());

            //Ленивая квантификация
        int counter = 0;
        String string = "196.198.1.197";
        Pattern pattern3 = Pattern.compile(".*?19");
        Matcher matcher4 = pattern3.matcher(string);
        while (matcher4.find()) {
            counter++;
            System.out.println(string.substring(matcher4.start(), matcher4.end()));
        }
        System.out.println(counter);


            //Жадная квантификация
        counter = 0;
        Pattern pattern5 = Pattern.compile(".*19");
        Matcher matcher6 = pattern5.matcher(string);
        while (matcher6.find()) {
            counter++;
            System.out.println(string.substring(matcher6.start(), matcher6.end()));
        }
        System.out.println(counter);


            //Сверхжадная квантификация
        counter = 0;
        Pattern pattern6 = Pattern.compile(".*+19");
        Matcher matcher7 = pattern6.matcher(string);
        while (matcher7.find()) {
            counter++;
            System.out.println(string.substring(matcher7.start(), matcher7.end()));
        }
        System.out.println(counter);
    }
}
